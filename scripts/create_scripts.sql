create table if not exists accounts (
	id bigserial primary key,
	username text not null unique
);

create table if not exists books (
	id bigserial primary key,
	title text not null,
	age_restriction int check(age_restriction in (0, 12, 14, 16, 18)),
	author_id bigint references accounts (id) on delete restrict not null
);

create table if not exists genres (
	id bigserial primary key,
	name text not null unique
);

create table if not exists genre_book (
	id bigserial primary key,
	book_id bigint references books (id) on delete cascade not null,
	genre_id bigint references genres (id) on delete cascade not null
);

alter table genre_book add constraint g_b_unique unique(book_id, genre_id);

create table if not exists chapters (
	id bigserial primary key,
	order_in_book int not null,
	name text not null,
	path_to_text text not null,
	book_id bigint references books (id) on delete cascade not null
);

alter table chapters add constraint ch_unique unique(order_in_book, book_id) deferrable initially deferred;

create table if not exists comments (
	id bigserial primary key,
	content text not null,
	time timestamp with time zone not null default now()::timestamp with time zone,
	account_id bigint references accounts (id) on delete set default default -1 not null,
	book_id bigint references books (id) on delete cascade,
	chapter_id bigint references chapters (id) on delete cascade
);
