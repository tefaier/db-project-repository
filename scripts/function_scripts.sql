/*
 * add a chapter to book so that it has certain position or at the end
 */
create or replace procedure insert_chapter(c_name text, c_path text, book_id_par bigint, place_to_put int default -1) as $$
declare 
	top_order int;
	chapters_count int;
	first_shift_order int;
begin 
	select max(c.order_in_book) into top_order from chapters as c where c.book_id = book_id_par;
	select count(*) into chapters_count from chapters as c where c.book_id = book_id_par;
	if place_to_put = -1 or place_to_put > chapters_count then 
		place_to_put := chapters_count + 1;
	end if;
	if place_to_put > chapters_count then 
		insert into chapters (order_in_book, "name", path_to_text, book_id) values (top_order + 1, c_name, c_path, book_id_par);
	else
		select min(c.order_in_book) into first_shift_order 
			from (
				select c.order_in_book 
				from chapters as c 
				where c.book_id = book_id_par 
				order by c.order_in_book 
				offset place_to_put - 1
			) as c;
		update chapters set order_in_book = order_in_book + 1 where order_in_book >= first_shift_order;
		insert into chapters (order_in_book, "name", path_to_text, book_id) values (first_shift_order, c_name, c_path, book_id_par);
	end if;
end;
$$
language plpgsql;

/*
 * return books with given genre
 */
create or replace function books_with_genre(genre_id bigint) returns setof books as $$
	select distinct b.*
	from books as b inner join genre_book as g_b on b.id = g_b.book_id inner join genres as g on g_b.genre_id = g.id
	where g.id = genre_id;
$$
language sql
stable;

/*
 * return books with given string in its name
 */
create or replace function books_with_name(to_match text) returns setof books as $$
	select distinct *
	from books
	where title ~* to_match;
$$
language sql
stable;

/*
 * return path as if there was a certain rule by which they are made
 */
create or replace function make_path(in book_id bigint, in chapter_name text) returns text as $$
declare 
	main_path text := 'server/chapters/';
begin 
	return main_path || book_id::text || '/' || chapter_name || '.txt';	
end;
$$
language plpgsql
immutable;

/*
 * add a deleted author marker to comments when its author is deleted
 */
create function get_authored_comment_content(username text, comment_content text) returns text as $$
begin 
	return username || ' : ' || comment_content;	
end;
$$
language plpgsql
immutable;

create or replace function set_comment_author_remove() returns trigger as $$
	declare 
		no_author_tag text := '(Пользователь удален) ';
    begin
        if new.account_id = -1 then
        	new.content = no_author_tag || new.content;
        end if;
    	return new;
    end;
$$
language plpgsql;

create trigger comment_unparent_content before update of account_id on "comments" for each row
execute function set_comment_author_remove();
