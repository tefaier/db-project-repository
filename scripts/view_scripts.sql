/*
 * view that returns accounts with connected books
 */
create or replace view accounts_books as
select a.id as account_id, a.username, b.id as book_id, b.title
from accounts a left join books b on a.id = b.author_id
where a.id <> -1
order by a.id;

/*
 * view that shows in what genres accounts write
 */
create or replace view accounts_genres as
select a.id as account_id, a.username, g.id as genre_id, g.name
from accounts a 
	inner join books b on a.id = b.author_id
	inner join genre_book gb on b.id = gb.book_id
	inner join genres g on gb.genre_id = g.id
order by a.id, g.id;

/*
 * view that returns books that or whose chapters were commented by account
 */
create or replace view commented_books as
select distinct a.id as account_id, a.username, b.id as book_id, b.title
from accounts a 
	inner join "comments" c on a.id = c.account_id
	left join chapters chap on c.chapter_id = chap.id
	left join books b on c.book_id = b.id or chap.book_id = b.id
where a.id <> -1
order by a.id, b.id;
	
/*
 * view that returns all comments by book and its chapters with time order
 */
create or replace view books_comments as
	select b.id as book_id, b.title, c.id as comment_id, c.content, c.time
	from books b 
		inner join "comments" c on b.id = c.book_id
union
	select b.id as book_id, b.title, c.id as comment_id, c.content, c.time
	from books b
		left join chapters chap on b.id = chap.book_id
		inner join "comments" c on chap.id = c.chapter_id
order by 1, 5;

/*
 * view that returns chapters writtent by each account
 * join on subquery to counter books that have zero chapters because it will create extra rows and make it harder to leave single 'fully' null rows for accounts that have no chapters written
 */
create or replace view account_chapters as
select a.id as account_id, a.username, b_c.book_id, b_c.chapter_id, b_c.chapter_name
from accounts a 
	left join (
		select b.author_id, b.id as book_id, c.id as chapter_id, c.name as chapter_name, c.order_in_book
		from books b inner join chapters c on b.id = c.book_id
	) as b_c on a.id = b_c.author_id
where a.id <> -1
order by a.id, b_c.book_id, b_c.order_in_book;
