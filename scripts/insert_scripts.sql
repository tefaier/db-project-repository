/*
 * ACCOUNTS
 * 
 * accounts filled -1, 1..11
 */
insert into accounts (username) 
values 
	('John'), ('Not John'), ('Also John'), ('Like a John'), ('Likes John'), 
	('Hates John'), ('Why a John'), ('Third John'), ('Super John'), ('So-so John'), 
	('John John');

/*
 * BOOKS
 * 
 * books taken as top 15 from website ranobelib.me
 * age_restriction can be only 0, 12, 14, 16, 18
 */
insert into books (title, age_restriction, author_id)
values
	('Точка зрения', 16, 1),
	('Ничтожество из графского', 16, 2),
	('Реинкарнация безработного', 16, 2),
	('Поднятие уровня', 16, 1),
	('Добро пожаловать', 0, 4),
	('Начало после конца', 16, 5),
	('Повелитель тайн', 16, 6),
	('Легендарный механик', 16, 7),
	('Ублюдок FFF-ранга', 18, 9),
	('Мать Ученья', 14, 5),
	('Злодей хочет жить', 12, 4),
	('Смерть - единственный конец', 18, 11);

/*
 * CHAPTERS
 * insert 4 chapters per book
 */
with book_ids as (
	select id
	from books
)
insert into chapters (order_in_book, "name", path_to_text, book_id) 
	select chap, 'Глава ' || chap::text, id::text || '/' || chap::text, id
	from book_ids cross join (values (1),(2),(3),(4)) chapters(chap);

/*
 * GENRES
 */
insert into genres ("name")
values 
	('fantasy'), ('romance'), ('adventure'), ('drama'), ('comedy'), 
	('action'), ('history'), ('game'), ('magic'), ('cosmos'), 
	('music'), ('horror');

/*
 * GENRE_BOOK
 */
insert into genre_book (book_id, genre_id)
	select b.id, g.id 
	from books as b cross join genres as g
	where random() > 0.8; 

/*
 * COMMENTS - 1
 */
with messages as (
	select '{Хорошо,Дизлайк отписка,Что это за бред,Отпуститееее меняяяя,Очень советую,Не очень советую}'::text[] texts
)
insert into "comments" ("content", "time", account_id, book_id)
	select 
		(select texts[a.id * 0 + 1 + floor((random() * array_length(texts, 1)))::int] from messages), 
		now()::timestamp with time zone - interval '3' day * random(), 
		a.id, 
		b.id
	from accounts a cross join books b
	where random() > 0.85;
	
/*
 * COMMENTS - 2
 */
with messages as (
	select '{Хорошо,Дизлайк отписка,Что это за бред,Отпуститееее меняяяя,Очень советую,Не очень советую,Топовая глава,Вот это поворот}'::text[] texts
)
insert into "comments" ("content", "time", account_id, chapter_id)
	select 
		(select texts[a.id * 0 + 1 + floor((random() * array_length(texts, 1)))::int] from messages), 
		now()::timestamp with time zone - interval '3' day * random(), 
		a.id, 
		c.id
	from accounts a cross join chapters c
	where random() > 0.95;
