/*
 * Move second chapter of book 5 to the end of book's order
 */
update chapters 
set order_in_book = (select max(order_in_book) + 1 from chapters where book_id = 5) 
where book_id = 5 and order_in_book = 2;

/*
 * delete of 18+ books
 */
delete from books 
where age_restriction = 18;

/*
 * delete of comments that were made more than year ago
 */
delete from "comments" 
where time + interval '1' year < now()::timestamp with time zone;

/*
 * update of genre names that are 'confusing'
 */
update genres 
set "name" = 'with ' || "name"
where "name" = any('{music,cosmos,game}'::text[]);

/*
 * delete of connections genre and book that exceed limit of 5
 */
delete from genre_book 
where id in (
	select sub.id
	from (
		select id, row_number() over w as row_n
		from genre_book 
		window w as (partition by book_id order by id)
	) as sub
	where sub.row_n > 5
);
