/*
 * select books with their number of genres, group 1
 */
select b.id, b.title, count(gb.id) as "number of genres"
from books b left join genre_book gb on b.id = gb.book_id 
group by b.id
order by b.id;

/*
 * select books' first chapter, window 1
 */
select c_ord.id, c_ord.order_in_book, c_ord."name", c_ord.path_to_text, c_ord.book_id
from (
	select c.id, c.order_in_book, c."name", c.path_to_text, c.book_id, row_number() over w as "order"
	from chapters c 
	window w as (partition by c.book_id order by c.order_in_book)
) as c_ord
where c_ord.order = 1;

/*
 * select accounts who have own any books, group 2
 */
select a.id, a.username 
from accounts a inner join books b on a.id = b.author_id 
group by a.id
order by a.id;

/*
 * select chapters with order being different more than by 1 from the previous, window 2
 */
select c_ord.id, c_ord.order_in_book, c_ord."name", c_ord.path_to_text, c_ord.book_id, c_ord."offset" as "offset from previous"
from (
	select c.id, c.order_in_book, c."name", c.path_to_text, c.book_id, c.order_in_book - (lag(order_in_book, 1, c.order_in_book) over w) as "offset"
	from chapters c 
	window w as (partition by c.book_id order by c.order_in_book)
) as c_ord
where c_ord."offset" > 1;

/*
 * select users who wrote no comments except special user, group 3
 */
select a.id, a.username
from accounts a left join "comments" c on a.id = c.account_id 
group by a.id
having count(c.id) = 0 and a.id <> -1
order by a.id;

/*
 * select books whose order in book and order by id aren't equal, window 3
 */
select c_ord.id, c_ord.order_in_book, c_ord."name", c_ord.path_to_text, c_ord.book_id
from (
	select c.id, c.order_in_book, c."name", c.path_to_text, c.book_id, row_number() over w_order as order, row_number() over w_time as order_id
	from chapters c 
	window w_order as (partition by c.book_id order by c.order_in_book), w_time as (partition by c.book_id order by c.id)
) as c_ord
where c_ord.order <> c_ord.order_id;

/*
 * select accounts and average length of their comments, group 4
 */
select a.id, a.username, round(avg(length(c."content")), 1) as average_comment_length
from accounts a inner join "comments" c on a.id = c.account_id 
where a.id <> -1
group by a.id;

/*
 * select comments each account made during last day of his activity, window 4
 */
select a_c.id, a_c.username, a_c.content
from (
	select a.id, a.username, c.content, (last_value(c.time) over w) - c.time as time_before_last
	from accounts a inner join "comments" c on a.id = c.account_id
	window w as (partition by a.id order by c.time)
) as a_c
where a_c.time_before_last < interval '1' day and a_c.id <> -1
order by a_c.time_before_last desc;

/*
 * select books with more than 5 chapters, group 5
 */
select b.id, b.title, count(c.id) as "number of chapters"
from books b left join chapters c on b.id = c.book_id 
group by b.id
having count(c.id) > 5
order by b.id;

/*
 * select comments and number of them that were made within 1 day by author (of a comment), window 5
 */
select a.id, a.username, c.content, count(c.id) over w as within_day_count
from accounts a inner join "comments" c on a.id = c.account_id
where a.id <> -1
window w as (partition by a.id order by c.time range between interval '1' day preceding and interval '1' day following)
order by a.id, c."time";
